import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import "../components"
import "../scripts/DashActions.js" as DashActions

Page {
    anchors.fill: parent
    id: dashPage

    property var searching: true
    property bool selectMode
    property var selectedList: new Object()
    signal selectAll()

    header: DefaultHeader {
        id: header
        title: i18n.tr('Jotit')
        sideStack: true

        leadingActionBar {
            actions: [
            Action {
                iconName: "filters"
                onTriggered: layout.pushPage ( "Settings" )
            }
            ]
        }

        trailingActionBar {
            actions: [
            Action {
                iconName: "add"
                onTriggered: DashActions.create ()
                visible: !selectMode
            },
            Action {
                visible: selectMode
                iconName: "close"
                onTriggered: selectMode = false
            },
            Action {
                visible: selectMode
                iconName: "select"
                onTriggered: selectAll()
            },
            Action {
                visible: selectMode
                iconName: "delete"
                onTriggered: {
                    for(var item in selectedList) {
                        notesModel.clear(item)
                    }
                    selectMode = false
                }
            }
            ]
        }

        contents: TextField {
            id: searchField
            objectName: "searchField"
            primaryItem: Icon {
                height: parent.height - units.gu(2)
                name: "find"
                anchors.left: parent.left
                anchors.leftMargin: units.gu(0.25)
            }
            width: parent.width - units.gu(2)
            anchors.centerIn: parent
            inputMethodHints: Qt.ImhNoPredictiveText
            placeholderText: i18n.tr("Search…")
            onDisplayTextChanged: notesModel.search ( displayText )
        }
    }

    Label {
        text: i18n.tr("You have no notes yet")
        anchors.centerIn: parent
        visible: noteListView.count === 0
    }


    ListView {
        id: noteListView
        width: parent.width
        height: parent.height - header.height
        anchors.top: header.bottom
        delegate: NoteListItem {}
        model: notesModel
        section.property: "dateSection"
        section.delegate: Rectangle {
            width: noteListView.width
            height: label.height + units.gu(4)
            color: theme.palette.normal.foreground
            Label {
                id: label
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: units.gu(2)
                text: section
            }
            ListItem {
                anchors.bottom: parent.bottom
                height: 1
            }
        }
        header: ListItem { height: 1 }
        PullToRefresh {
            refreshing: notesModel.refreshing > 0
            onRefresh: notesModel.sync()
        }
    }
}
