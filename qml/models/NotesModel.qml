import QtQuick 2.4
import QtQuick.LocalStorage 2.0 as Database
import "../scripts/StorageActions.js" as StorageActions

ListModel {
    id: model

    property var currentNodeId: ""

    property string nextcloudDomain
    property string nextcloudUsername
    property string nextcloudPassword
    property string url: "%1/index.php/apps/notes/api/v0.2/notes".arg(nextcloudDomain)
    property int refreshing: 0

    function sync() {
        if (nextcloudDomain === null 
            || nextcloudUsername === null 
            || nextcloudPassword === null) return
        var http = new XMLHttpRequest()
        http.open('GET', url)
        http.setRequestHeader("Authorization", "Basic " + Qt.btoa(nextcloudUsername + ":" + nextcloudPassword))
        refreshing++
        http.onreadystatechange = function() {
            if (http.readyState === XMLHttpRequest.DONE) refreshing--
            if (http.readyState !== XMLHttpRequest.DONE || http.status !== 200) return
            var remoteList
            try {
                remoteList = JSON.parse(http.responseText)
            }
            catch(e) {
                return toast.show(i18n.tr("Something went wrong: %1").arg(e))
            }
            console.log("Response! We have got %1 remote notes".arg(remoteList.length))

            var handledIds = new Object()
            
            for(var i = 0; i < remoteList.length; i++) {
                var remoteItem = remoteList[i]
                var localeId = getViewIDByID(remoteItem.id)
                if (handledIds[remoteItem.id]) return
                var localeItem = localeId !== null ? model.get(localeId) : null
                compareAndSync(remoteItem, localeItem)
                handledIds[remoteItem.id] = true
            }
            for(var i = 0; i < model.count; i++) {
                var localeItem = model.get(i)
                if (handledIds[localeItem.id]) return
                var remoteItem = null
                for(var j = 0; j < remoteList.length; j++)
                    if(remoteList[j].id === localeItem.id) break
                if (j < remoteList.length) remoteItem = remoteList[j]
                compareAndSync(remoteItem, localeItem)
                handledIds[localeItem.id] = true
            }
        }
        http.send()
    }

    function compareAndSync (remoteItem,localeItem) {
        if (localeItem === undefined || localeItem === null) {
            console.log("Locale item not found. Add it to locale database!")
            add(remoteItem.content, remoteItem.id, remoteItem.modified*1000)
            toast.show(i18n.tr("New note has been downloaded from the cloud."))
        }
        else if (remoteItem === undefined || remoteItem === null) {
            console.log("Remote item not found. Upload it!")
            var http = new XMLHttpRequest()
            http.open("POST", url)
            http.setRequestHeader('Content-type', 'application/json; charset=utf-8')
            http.setRequestHeader("Authorization", "Basic " + Qt.btoa(nextcloudUsername + ":" + nextcloudPassword))
            refreshing++
            http.onreadystatechange = function() {
                if (http.readyState === XMLHttpRequest.DONE) refreshing--
                if (http.readyState !== XMLHttpRequest.DONE || http.status !== 200) return
                var remoteList
                try {
                    remoteList = JSON.parse(http.responseText)
                }
                catch(e) {
                    return toast.show(i18n.tr("Something went wrong: %1").arg(e))
                }
                StorageActions.transaction ( 'UPDATE Notes SET id=? WHERE id=?', [
                remoteList.id,
                localeItem.id
                ] )
                model.set ( getViewIDByID ( localeItem.id ), { "id": remoteList.id })
            }
            http.send(JSON.stringify({
                "content": localeItem.text,
                "modified": localeItem.timestamp/1000
            }))
        }
        else if((localeItem.timestamp - remoteItem.modified*1000) < -1000) {
            print("Locale item is outdated! Update it!")
            update(remoteItem.id, remoteItem.content, remoteItem.modified*1000)
            toast.show(i18n.tr("Notes have been updated."))
        }
        else if((localeItem.timestamp - remoteItem.modified*1000) > 1000) {
            console.log("Remote item is outdated! Update it!", localeItem.timestamp/1000, remoteItem.modified)
            var http = new XMLHttpRequest()
            http.open("PUT", url + "/" + remoteItem.id)
            http.setRequestHeader("Authorization", "Basic " + Qt.btoa(nextcloudUsername + ":" + nextcloudPassword))
            http.setRequestHeader('Content-type', 'application/json; charset=utf-8')
            refreshing++
            http.onreadystatechange = function() {
                if (http.readyState === XMLHttpRequest.DONE) refreshing--
            }
            http.send(JSON.stringify({
                "content": localeItem.text,
                "modified": localeItem.timestamp/1000
            }))
            console.log("Sent to:",url + "/" + remoteItem.id)
        }
    }

    function add ( text, id, timestamp ) {
        var now = new Date().getTime()
        if (timestamp == null) timestamp = now
        if (id == null) id = now
        StorageActions.transaction('INSERT INTO Notes VALUES(?, ?, ?)', [
        id,
        text,
        timestamp
        ])
        model.insert ( 0, {
            "id": id,
            "text": text,
            "timestamp": timestamp,
            "isVisible": true,
            "dateSection": new Date(timestamp).toLocaleDateString(Qt.locale())
        } )
        // Reorder the list
        var n = 0
        while ( n < model.count-1 && model.get( n ).timestamp < model.get( n+1 ).timestamp ) n++
        if (n > 0) model.move( 0, n, 1 )
        return now
    }


    function update ( id, text, timestamp ) {
        var now = new Date().getTime()
        if (timestamp === null) timestamp = now
        StorageActions.transaction ( 'UPDATE Notes SET text=?, timestamp=? WHERE id=?', [
        text,
        timestamp,
        id
        ] )
        var n = getViewIDByID ( id )
        model.set ( n, { "text": text, "timestamp": timestamp })

        var oldN = n
        // Reorder the list
        while ( n > 0 && model.get( n ).timestamp > model.get( n-1 ).timestamp ) n--
        model.move( oldN, n, 1 )
    }


    function clear ( id ) {
        StorageActions.transaction ( 'DELETE FROM Notes WHERE id=?', [ id ] )
        model.remove ( getViewIDByID ( id ) )
        if (nextcloudDomain === null 
            || nextcloudUsername === null 
            || nextcloudPassword === null) return
        var http = new XMLHttpRequest()
        http.open("DELETE", url + "/" + id)
        http.setRequestHeader("Authorization", "Basic " + Qt.btoa(nextcloudUsername + ":" + nextcloudPassword))
        refreshing++
        http.onreadystatechange = function() {
            if (http.readyState === XMLHttpRequest.DONE) refreshing--
        }
        http.send()
    }


    function getViewIDByID ( id ) {
        for ( var i = 0; i < model.count; i++ ) {
            if ( model.get(i).id === id ) return i
        }
        return null
    }


    function getNote ( id, callback ) {
        var handleResult = function ( result ) {
            if ( result.rows.length !== 1 ) callback ( false )
            else callback ( result.rows[0] )
        }

        StorageActions.transaction ( 'SELECT text, timestamp FROM Notes WHERE id=?', [ id ], handleResult )
    }


    function search ( str ) {
        var searchStr = str.toUpperCase()
        for ( var i = 0; i < model.count; i++ ) {
            var visible = model.get(i).text.toUpperCase().indexOf( searchStr ) !== -1
            model.set( i, { "isVisible": visible })
        }
    }


    function _init() {

        var updateList = function ( rs ) {
            model.clear()
            for (var i = 0; i < rs.rows.length; i++) {
                var item = rs.rows.item(i)
                item.isVisible = true
                item.dateSection = new Date(item.timestamp).toLocaleDateString(Qt.locale())
                model.append ( item )
            }
        }

        StorageActions.transaction ( 'CREATE TABLE IF NOT EXISTS Notes( id INTEGER, text TEXT, timestamp INTEGER, UNIQUE (id) )' )
        StorageActions.transaction ( 'SELECT * FROM Notes ORDER BY timestamp DESC', [], updateList )
        sync()
    }

    Component.onCompleted: _init ()
}
