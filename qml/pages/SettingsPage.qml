import QtQuick 2.9
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import "../components"
import "../config.js" as Config

Page {
    anchors.fill: parent
    id: page

    property bool connecting: false

    header: DefaultHeader {
        id: header
        title: i18n.tr('Settings')
        contents: Sections {
            anchors {
                left: parent.left
                bottom: parent.bottom
            }
            selectedIndex: 0
            model: [ i18n.tr("Nextcloud settings"), i18n.tr("About Jotit")]
            onSelectedIndexChanged: {
                if(selectedIndex === 1) layout.pushPage ( "Info" )
            }
        }
    }


    ScrollView {
        id: scrollView
        width: parent.width
        height: parent.height - header.height
        anchors.top: header.bottom
        contentItem: Column {
            width: page.width
            spacing: units.gu(2)

            Rectangle {
                height: units.gu(2)
                width: units.gu(20)
                color: "transparent"
            }
            
            TextField {
                id: nextcloudDomainTextField
                placeholderText: i18n.tr("https://your-nextcloud.domain")
                text: notesModel.nextcloudDomain || ""
                readOnly: connecting
                width: units.gu(30)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            TextField {
                id: nextcloudUsernameTextField
                placeholderText: i18n.tr("Username")
                text: notesModel.nextcloudUsername || ""
                readOnly: connecting
                width: units.gu(30)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            TextField {
                id: nextcloudPasswortTextField
                placeholderText: i18n.tr("Password")
                text: notesModel.nextcloudPassword || ""
                echoMode: TextInput.Password
                readOnly: connecting
                width: units.gu(30)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Button {
                text: i18n.tr("Connect")
                enabled: !connecting && nextcloudDomainTextField.text !== ""
                width: units.gu(30)
                ActivityIndicator{
                    anchors.centerIn: parent
                    visible: connecting
                    running: connecting
                }
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: {
                    connecting = true
                    var url = "%1/index.php/apps/notes/api/v0.2/notes?exclude=content,title".arg(nextcloudDomainTextField.text)
                    var http = new XMLHttpRequest()
                    http.open('GET', url)
                    http.setRequestHeader("Authorization", "Basic " + Qt.btoa(nextcloudUsernameTextField.text + ":" + nextcloudPasswortTextField.text))
                    http.onreadystatechange = function() {
                        if (http.readyState === XMLHttpRequest.DONE) connecting = false
                        if (http.readyState === XMLHttpRequest.DONE && http.status !== 200) {
                            toast.show(i18n.tr("Oops! No connection possible..."))
                            return
                        }
                        notesModel.nextcloudDomain = nextcloudDomainTextField.text
                        notesModel.nextcloudUsername = nextcloudUsernameTextField.text
                        notesModel.nextcloudPassword = nextcloudPasswortTextField.text
                        toast.show(i18n.tr("Jotit is now connected to your Nextcloud!"))
                        notesModel.sync()
                    }
                    http.send()
                }
                color: LomiriColors.green
            }

        }
    }

}
